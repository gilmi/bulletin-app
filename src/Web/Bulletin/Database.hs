{- | Database access

We'll put all of the database access related functionality here

-}


{-# LANGUAGE OverloadedStrings #-}

module Web.Bulletin.Database
  ( module Web.Bulletin.Database
  , module Export
  )
where

import Data.Int (Int64)
import Control.Monad (void)
import Control.Monad.Reader (ReaderT)
import qualified Database.Persist.Sql as P
import qualified Database.Persist.Sqlite as PSqlite3

import Web.Bulletin.Config
import Web.Bulletin.Model
import qualified Web.Scotty.Sqlite.Users as Users

import Web.Bulletin.Database.Common as Export
import Web.Bulletin.Database.Users as Export
import Web.Bulletin.Database.Posts as Export
import Web.Bulletin.Database.RegTokens as Export

-------------
-- General --
-------------

-- | Create and update the required tables for this website to run
dbMigrations :: Config -> IO ()
dbMigrations cfg = do
  -- Hacking around this bug in persistent-sqlite:
  -- https://github.com/yesodweb/persistent/issues/1125
  let
    runRawNoTransaction sql =
      void $ P.runSqlPoolNoTransaction
        (PSqlite3.rawSql sql [] :: ReaderT PSqlite3.SqlBackend IO [P.Single Int64])
        (Users.siPool (cfgUsersStateInfo cfg))
        Nothing

  runRawNoTransaction "PRAGMA foreign_keys=OFF"
  -- migrations for Users
  Users.dbMigrations (Users.siPool (cfgUsersStateInfo cfg))

  -- migrations for the rest of the bulletin board
  _ <- runDB cfg $ do
    PSqlite3.runMigration migrateAll ---- migration
    PSqlite3.rawSql "PRAGMA foreign_keys_check" [] :: ReaderT PSqlite3.SqlBackend IO [P.Single Int64]

  runRawNoTransaction "PRAGMA foreign_keys=ON"


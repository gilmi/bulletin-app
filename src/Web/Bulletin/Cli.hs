{- | Command line application interface. This is the entry point of the application.

-}

{-# LANGUAGE DataKinds, DeriveGeneric, FlexibleInstances, OverloadedStrings, StandaloneDeriving, TypeOperators #-} -- for optparse-generic

{-# language NamedFieldPuns #-}
{-# language OverloadedStrings #-}

module Web.Bulletin.Cli where

import Options.Generic
import Data.Int (Int64)
import Data.Bool (bool)
import Data.List (transpose)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.IO as TL
import System.IO
import System.Exit

import qualified Database.Persist.Sql as P
import qualified Web.Scotty.Sqlite.Users as Users
import qualified Web.Scotty.Sqlite.Users.ChangePassword as Users
import qualified Web.Bulletin.Database as DB
import Web.Bulletin.Config
import Web.Bulletin.Model
import Web.Bulletin.Server (run)

-- | Initialize database and datastructures according to the environment
--   and return the configuration.
setup :: IO Config
setup = do
  ses <- Users.initSessionStore
  cfg <- getConfig ses
  DB.dbMigrations cfg
  pure cfg

-- | Entry point to the program. Handle user commands.
cli :: IO ()
cli = do
  command <- unwrapRecord "Bulletin board website"
  cfg <- setup
  case command of
    Serve -> do
      run cfg

    List_users -> do
      TL.putStr . showUsers =<< Users.getAllUsers (cfgUsersStateInfo cfg)

    Generate_invites { amount } -> do
      _ <- DB.generateTokens' cfg (P.toSqlKey 0) amount
      tokens <- DB.getTokens' cfg (P.toSqlKey 0)
      T.putStrLn "Available tokens for you:"
      mapM_ (T.putStrLn . (<>) "/register/") tokens

    Reset_user_password { username, password } -> do
      muser <- Users.getLoginByUsername (cfgUsersStateInfo cfg) username
      case muser of
        Nothing -> do
          T.hPutStrLn stderr "User does not exist"
          exitFailure
        Just (P.Entity uid _) -> do
          Users.changePass (cfgUsersStateInfo cfg) uid password

    Lock_post { post_id, unlock } -> do
      DB.unsafeLockPostIO cfg (P.toSqlKey post_id) (not unlock)

    Hide_post { post_id, unhide } -> do
      DB.unsafeHidePostIO cfg (P.toSqlKey post_id) (not unhide)

    Grant_moderator { username, revoke } -> do
      DB.unsafeSetPrivByUsernameIO cfg (bool Mod Normal revoke) username

data Command w
  = Serve
  | List_users
  | Generate_invites { amount :: w ::: Int <?> "How many invite to generate" }
  | Reset_user_password { username :: TL.Text, password :: T.Text }
  | Lock_post { post_id :: Int64, unlock :: Bool }
  | Hide_post { post_id :: Int64, unhide :: Bool }
  | Grant_moderator { username :: TL.Text, revoke :: Bool }
--  | Ban_user { username :: TL.Text }
--  | Reset_user_info { username :: TL.Text }
    deriving Generic

instance ParseRecord (Command Wrapped)
deriving instance Show (Command Unwrapped)

showUsers :: [P.Entity Users.Login] -> TL.Text
showUsers list =
  let
    toprowfields = ["user_id", "username"]
    fields = map showUserFields list
    maxlens = map (maximum . map TL.length) $ zipWith (:) toprowfields $ transpose fields
    toprow = showRow ' ' maxlens toprowfields
    seperator = "|-" <> TL.intercalate "-+-" (zipWith (pad '-') maxlens (map (const "") toprowfields)) <> "-|"
  in
    TL.unlines $ toprow : seperator : map (showRow ' ' maxlens) fields
    
showRow :: Char -> [Int64] -> [TL.Text] -> TL.Text
showRow c maxlens fields =
  wrapBlock $ TL.intercalate " | " $ zipWith (pad c) maxlens fields

wrapBlock :: TL.Text -> TL.Text
wrapBlock a = "| " <> a <> " |"

pad :: Char -> Int64 -> TL.Text -> TL.Text
pad char maxlen field = field <> TL.replicate (maxlen - TL.length field) (TL.singleton char)

showUserFields :: P.Entity Users.Login -> [TL.Text]
showUserFields (P.Entity uid user) =
  [ TL.pack $ show $ P.fromSqlKey uid
  , Users.loginUsername user
  ]

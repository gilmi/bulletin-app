{- | Common functions for validation
-}

module Web.Bulletin.Validation where

applyMaybe :: Applicative m => Maybe a -> (a -> m ()) -> m ()
applyMaybe m f =
  case m of
    Nothing -> pure ()
    Just x -> f x


{- | Scotty server and middlewares. This is the entry point for the website server.

-}

{-# language OverloadedStrings #-}

module Web.Bulletin.Server where

import qualified Network.Wai as Wai
import qualified Network.Wai.Middleware.RequestLogger as WaiLog
import qualified Web.Scotty as S
import qualified Data.Text.Lazy as TL
import qualified Network.HTTP.Types as HTTP
import qualified Lucid as H

import Web.Bulletin.Config
import Web.Bulletin.Html
import Web.Bulletin.Router

---------------
-- * Runner

-- | Run the scotty server.
run :: Config -> IO ()
run cfg = do
  S.scotty (cfgPort cfg) (myApp cfg)

-- | Scotty server app. Can be used to run a server with different WAI settings
--
--   The @Config@ can be provided by @Web.Bulletin.Cli.setup@, and the @ScottyM ()@ can be
--   run with different settings by using
--   [functions from scotty](https://hackage.haskell.org/package/scotty-0.12/docs/Web-Scotty.html#g:1)
myApp :: Config -> S.ScottyM ()
myApp cfg = do
  S.middleware (loggingM (cfgEnv cfg))
  S.defaultHandler (defaultH (cfgEnv cfg))
  router cfg

----------------------
-- ** Middlewares

-- | Request logging
loggingM :: Environment -> Wai.Middleware
loggingM e = case e of
  Development -> WaiLog.logStdoutDev
  Production -> WaiLog.logStdout
  Testing -> id

-- | Default request handler in can something failed
defaultH :: Environment -> TL.Text -> S.ActionM ()
defaultH env err = do
  S.status HTTP.internalServerError500
  S.html $
    H.renderText $
      template
        ("Internal Server Error 500")
        ""
        $ case env of
          Development ->
            H.toHtml err
          Testing ->
            H.toHtml err
          Production ->
            ""

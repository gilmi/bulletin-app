{- | Defining a website model using persistent

-}

{-# language TemplateHaskell #-}
{-# language QuasiQuotes #-}
{-# language TypeFamilies #-}
{-# language GADTs #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}

module Web.Bulletin.Model where

import qualified Data.Time.Clock as C
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import qualified Database.Persist as P
import qualified Database.Persist.Sql as P
import Database.Persist.TH

import qualified Web.Scotty.Sqlite.Users.Model as Users

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Post
    title TL.Text
    content TL.Text
    date C.UTCTime
    authorId Users.LoginId
    locked Bool default=False
    hidden Bool default=False
    deriving Show

Moderator
    modId Users.LoginId
    ModId modId

UserDetails
    loginId Users.LoginId
    about TL.Text
    LoginId loginId

RegistrationToken
    userId Users.LoginId
    token T.Text
    Token token
    deriving Show

MaxRegistrationTokens
    amount Int
    userId Users.LoginId
    UserId userId
    deriving Show

Comment
    content TL.Text
    date C.UTCTime
    postId PostId
    authorId Users.LoginId
    deriving Show
|]

type User' = P.Entity Users.Login
type Comment' = P.Entity Comment
type PostBrief = (P.Entity Post, P.Entity Users.Login, Int)
type PostWithComments = (PostBrief, [(Comment', User')])
type Posts = [PostBrief]
type Logins = [P.Entity Users.Login]
type Comments = [(Comment', User')]
type UserLevel = Int
data UserInfo
  = UserInfo
  { uiLogin :: Users.Login
  , uiDetails :: UserDetails
  , uiLevel :: UserLevel
  , uiPrivilege :: UserPrivilege
  }

data UserPrivilege
  = Normal
  | Mod
  deriving (Eq, Ord, Enum, Bounded)

ppPriv :: UserPrivilege -> String
ppPriv priv =
  case priv of
    Normal -> "normal user"
    Mod -> "moderator"

showPostKey :: PostId -> TL.Text
showPostKey pk =
  case head $ P.keyToValues pk of
    P.PersistInt64 i -> TL.pack (show i)
    v -> error $ "Unexpected result for keyToValues in showPostKey: " <> show v

emptyUserDetails :: Users.LoginId -> UserDetails
emptyUserDetails = flip UserDetails ""

{- | Post management

-}


{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Web.Bulletin.Database.Posts where

import Data.Maybe (listToMaybe)
import qualified Data.Text.Lazy as TL
import qualified Database.Persist as P
import qualified Database.Persist.Sql as P
import qualified Database.Persist.Sqlite as PSqlite3
import Text.RawString.QQ

import Web.Bulletin.Config
import Web.Bulletin.Model
import qualified Web.Scotty.Sqlite.Users as Users

import Web.Bulletin.Database.Common
import Web.Bulletin.Database.Users (getPrivilege)


-----------
-- Posts --
-----------

-- ** Posts related functions

-- | Get all of the posts excluding the hidden posts, including the authors information
getAllPosts :: Config -> IO Posts
getAllPosts cfg = do
  fmap (fmap (fmap P.unSingle)) . runDB cfg $
    PSqlite3.rawSql
      [r|
select ??, ??, (select count(*) from comment where post.id = post_id)
from login inner join post
  on login.id = post.author_id
where post.hidden = false
order by post.id desc;
      |]
      []

-- | Get post without the comments
getPostBrief :: PostId -> Sql (Maybe PostBrief)
getPostBrief pid = do
  fmap (fmap P.unSingle) . listToMaybe <$>
    PSqlite3.rawSql
      [r|
select ??, ??, (select count(*) from comment where post.id = post_id)
 from login inner join post
   on login.id = post.author_id
where post.id = ?
order by post.id desc;
      |]
      [head $ P.keyToValues pid]

-- | Get post with the comments
getPostWithComments :: PostId -> Sql (Maybe PostWithComments)
getPostWithComments pid = do
  mpost <- getPostBrief pid
  maybe
    (pure Nothing)
    ( \post -> do
      comments <- getCommentsByPostId pid
      pure $ Just (post, comments)
    )
    mpost

-- | Get comments
getCommentsByPostId :: PostId -> Sql Comments
getCommentsByPostId pid = do
  PSqlite3.rawSql
    "select ??, ?? from login inner join comment on login.id = comment.author_id where comment.post_id = ? order by comment.id asc;"
    [head $ P.keyToValues pid]

-- | Get all posts by a particular author
getAuthorPosts :: Config -> Users.LoginId -> IO Posts
getAuthorPosts cfg uid = do
  fmap (fmap (fmap P.unSingle)) . runDB cfg $
    PSqlite3.rawSql
      [r|
select ??, ??, (select count(*) from comment where post.id = post_id)
 from login inner join post
   on login.id = post.author_id
where login.id = ? and post.hidden = false
order by post.id desc;
      |]
      [head (P.keyToValues uid)]

-- | Get all posts by a particular user, search by username
getAuthorPostsByUsername :: Config -> Users.Username -> IO Posts
getAuthorPostsByUsername cfg username = do
  fmap (fmap (fmap P.unSingle)) . runDB cfg $
    PSqlite3.rawSql
      [r|
select ??, ??, (select count(*) from comment where post.id = post_id)
 from login inner join post
   on login.id = post.author_id
where login.username = ? and post.hidden = false
order by post.id desc;
      |]
      [P.PersistText $ TL.toStrict username]

-- | Get all comments by a particular user, search by username
getAuthorCommentsByUsername :: Config -> Users.Username -> IO [(PostId, TL.Text, Comment')]
getAuthorCommentsByUsername cfg username = do
  runDB cfg $ do
    muser <- P.getBy (Users.Username username)
    case muser of
      Nothing ->
        pure []
      Just (P.Entity uid _) -> do
        results <- PSqlite3.rawSql
          "select post.id, post.title, comment.* from post inner join comment on post.id = comment.post_id where post.hidden = false and comment.author_id = ?;"
          [head $ P.keyToValues uid]
        pure $ fmap (\(pid, P.Single title, comment') -> (pid, title, comment')) results

-----------------------------------------
-- *** Adding, Editing and Deleting Posts

-- | Insert a new post
insertPost :: Config -> Post -> IO (P.Key Post)
insertPost cfg = runDB cfg . P.insert

-- | Insert a new comment
insertComment :: Config -> Comment -> IO (P.Key Comment)
insertComment cfg = runDB cfg . P.insert

-- | Represent the result for trying to edit a post
data EditPostStatus
  = Success -- ^ Post was updated
  | FailDoesn'tExist -- ^ Update failed because the post doesn't exist
  | FailNotAllowed -- ^ Update failed because the user is not allowed to update it

-- | Check if the user is allowed to update a post
--
--   The user can update a post if:
--   1. It's their post and the post isn't locked, or
--   2. The user is a moderator
--
checkPost :: P.Entity Users.Login -> PostId -> Sql EditPostStatus
checkPost user@(P.Entity uid _) pid = do
  mpost <- getPostBrief pid
  priv <- getPrivilege user
  case mpost of
    Just (P.Entity _ Post{postAuthorId=aid, postLocked=locked}, _, _)
      | (uid == aid && not locked) || priv == Mod -> do
        pure Success
      | otherwise ->
        pure FailNotAllowed
    Nothing ->
      pure FailDoesn'tExist
  
editPost :: Config -> P.Entity Users.Login -> PostId -> TL.Text -> TL.Text -> IO EditPostStatus
editPost cfg user pid title content =
  runDB cfg $ do
    status <- checkPost user pid
    case status of
      Success ->
        P.update pid
          [ PostTitle PSqlite3.=. title
          , PostContent PSqlite3.=. content
          ]
      _ ->
        pure ()
    pure status

deletePost :: Config -> P.Entity Users.Login -> PostId -> IO EditPostStatus
deletePost cfg user pid =
  runDB cfg $ do
    status <- checkPost user pid
    case status of
      Success ->
        P.delete pid
      _ ->
        pure ()
    pure status

-- | Lock a post. Only a moderator can do this
lockPost :: Config -> P.Entity Users.Login -> PostId -> Bool -> IO EditPostStatus
lockPost cfg user pid lockstatus =
  runDB cfg $ modPostSql user pid $ P.update pid
    [ PostLocked PSqlite3.=. lockstatus
    ]

-- | Hide a post. Only a moderator can do this
hidePost :: Config -> P.Entity Users.Login -> PostId -> Bool -> IO EditPostStatus
hidePost cfg user pid hidestatus =
  runDB cfg $ modPostSql user pid $ P.update pid
    [ PostHidden PSqlite3.=. hidestatus
    ]

-- | Update a post as a moderator
modPostSql :: P.Entity Users.Login -> PostId -> Sql () -> Sql EditPostStatus
modPostSql user pid sql = do
  mpost <- getPostBrief pid
  priv <- getPrivilege user
  case mpost of
    Just{}
      | priv == Mod -> do
        sql
        pure Success
      | otherwise ->
        pure FailNotAllowed
    Nothing ->
      pure FailDoesn'tExist

-- | Lock a post without checking for permissions!
unsafeLockPostIO :: Config -> PostId -> Bool -> IO ()
unsafeLockPostIO cfg pid lock = do
  runDB cfg $ P.update pid
    [ PostLocked PSqlite3.=. lock
    ]

-- | Hide a post without checking for permissions!
unsafeHidePostIO :: Config -> PostId -> Bool -> IO ()
unsafeHidePostIO cfg pid hide = do
  runDB cfg $ P.update pid
    [ PostHidden PSqlite3.=. hide
    ]


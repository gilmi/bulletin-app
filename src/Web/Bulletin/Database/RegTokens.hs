{- | Database registration invite tokens

New users can be invite to the board by an existing user.

We generate registration tokens according to the user level. See the Pure Functions section.

-}


{-# LANGUAGE OverloadedStrings #-}

module Web.Bulletin.Database.RegTokens where

import Control.Monad (void)
import qualified Data.Text as T
import qualified Database.Persist as P
import qualified Database.Persist.Sql as P
import qualified Database.Persist.Sqlite as PSqlite3

--
import Data.Int (Int64)
import qualified Data.ByteString.Lazy.Char8 as BSLC
import qualified Web.Scotty.Sqlite.Users as Users
import qualified Test.RandomStrings as RS
import Data.Digest.Pure.MD5 (md5)
--

import Web.Bulletin.Config
import Web.Bulletin.Model

import Web.Bulletin.Database.Common

-------------------------
-- Registration Tokens --
-------------------------

{- * Registration Tokens

These are used to invite a user into the bulletin board.

-}

--------------
-- ** API

-- | Generate and return all available registration tokens according to the user level
getTokensByUserId :: Config -> Users.LoginId -> IO [T.Text]
getTokensByUserId cfg uid = do
  generateTokens cfg uid
  getTokens' cfg uid

-- | Check if a registration token exists in the database
isValidToken :: Config -> T.Text -> IO Bool
isValidToken cfg =
  fmap (maybe False (const True)) . runDB cfg . P.getBy . Token

-- | Delete a registration token from the database
deleteToken :: Config -> T.Text -> IO ()
deleteToken cfg =
  runDB cfg . P.deleteBy . Token

-------------------------
-- ** Implementation

-- | Generate new tokens.
--
--   Tokens are only created when the users requests them to be generated.
--   We remember how many tokens were generated in the database
--   and generate new ones according to the user level.
--
generateTokens :: Config -> Users.LoginId -> IO ()
generateTokens cfg uid = do
  numOfTokensToGenerate <- runDB cfg $ do
    mCurrentMaxTokensEntry <- getCurrentMaxTokensAmount uid
    expectedMaxTokens <- calcMaxTokenAmount uid
    case mCurrentMaxTokensEntry of
      Nothing -> do
        void $ P.insert $ MaxRegistrationTokens expectedMaxTokens uid
        pure expectedMaxTokens
      Just currentMaxTokensEntry -> do
        P.update
          (P.entityKey currentMaxTokensEntry)
          [ MaxRegistrationTokensAmount PSqlite3.=. expectedMaxTokens ]
        let
          currentMaxTokens =
            maxRegistrationTokensAmount (P.entityVal currentMaxTokensEntry)
          numOfTokensToGenerate =
            expectedMaxTokens - currentMaxTokens
        pure numOfTokensToGenerate
  void $ generateTokens' cfg uid numOfTokensToGenerate

generateTokens' :: Config -> Users.LoginId -> Int -> IO [T.Text]
generateTokens' cfg uid amount = do
  tokens <- generateRandomTokens amount uid
  runDB cfg $
    mapM_ (P.insert . RegistrationToken uid) tokens
  pure tokens

getTokens' :: Config -> Users.LoginId -> IO [T.Text]
getTokens' cfg uid = do
  results <- runDB cfg $
    P.selectList [RegistrationTokenUserId P.==. uid] []
  pure $ registrationTokenToken . P.entityVal <$> results
  

getCurrentMaxTokensAmount :: Users.LoginId -> Sql (Maybe (P.Entity MaxRegistrationTokens))
getCurrentMaxTokensAmount uid = P.getBy (UserId uid)

-- | Calculate the level of the user according to their activity.
getLevel :: Users.LoginId -> Sql Int
getLevel uid = calcLevel <$> getActivity uid

-- | calculate how many users a user can invite according to their level
calcMaxTokenAmount :: Users.LoginId -> Sql Int
calcMaxTokenAmount uid = do
  tokensPerLevel <$> getLevel uid

getActivity :: Users.LoginId -> Sql Int
getActivity uid = do
  P.unSingle . head <$> PSqlite3.rawSql
    "select (select count(*) from post where author_id = ?) + (select count(*) from comment where author_id = ?)"
    [head (P.keyToValues uid), head (P.keyToValues uid)]

------------------------
-- ** Pure functions

generateRandomTokens :: Int -> Users.LoginId -> IO [T.Text]
generateRandomTokens amount name =
  zipWith (<>) (repeat (shortmd5 name))
    <$> fmap (fmap T.pack) (RS.randomStrings (RS.randomString (RS.onlyAlphaNum RS.randomASCII) 32) amount)

shortmd5 :: Users.LoginId -> T.Text
shortmd5 = T.pack . take 8 . show . md5 . BSLC.pack . show

tokensPerLevel :: Int -> Int
tokensPerLevel level = level * 2

calcLevel :: Int -> Int
calcLevel amount = floor ((fromIntegral amount / 10) ** (1 / 1.6))

fromLevel :: Int -> Int
fromLevel n = floor (10 * (fromIntegral n ** 1.6)) + 1

unsafeExtractInt :: P.PersistValue -> Int64
unsafeExtractInt val =
  case val of
    P.PersistInt64 i -> i
    v -> error $ "Unexpected result for keyToValues in showPostKey: " <> show v

{- | User management

We'll put all of the database access related functionality here

-}


{-# LANGUAGE OverloadedStrings #-}

module Web.Bulletin.Database.Users where

import Control.Monad (void)
import qualified Data.Text.Lazy as TL
import qualified Database.Persist as P
import qualified Database.Persist.Sqlite as PSqlite3

import Web.Bulletin.Config
import Web.Bulletin.Model
import qualified Web.Scotty.Sqlite.Users as Users

import Web.Bulletin.Database.Common
import Web.Bulletin.Database.RegTokens (getLevel)


-----------
-- Users --
-----------

-- ** Users

-- | Get user by login id
getUserById :: Config -> Users.LoginId -> IO (Maybe (P.Entity Users.Login))
getUserById cfg uid = runDB cfg $ fmap (P.Entity uid) <$> P.get uid

-- | Get user by username
getUserInfoByUsername :: Config -> Users.Username -> IO (Maybe UserInfo)
getUserInfoByUsername cfg username =
  runDB cfg $ traverse mkUserInfoSql =<< P.getBy (Users.Username username)

-- | Get user information by login id
getUserInfoById :: Config -> Users.LoginId -> IO (Maybe UserInfo)
getUserInfoById cfg uid =
  runDB cfg $ traverse (mkUserInfoSql . P.Entity uid) =<< P.get uid

-- | Fetch and/or calculate user information given a user login information
mkUserInfoSql :: P.Entity Users.Login -> Sql UserInfo
mkUserInfoSql euser@(P.Entity uid user) = do
  mdetails <- fmap P.entityVal <$> P.getBy (LoginId uid)
  level <- getLevel uid
  privilege <- getPrivilege euser
  pure $ UserInfo
    { uiLogin = user
    , uiDetails = maybe (emptyUserDetails uid) id mdetails
    , uiLevel = level
    , uiPrivilege = privilege
    }

-- | Update the display name and about info of a user
updateUserInfo :: Config -> Users.LoginId -> Users.Displayname -> TL.Text -> IO ()
updateUserInfo cfg uid displayname about =
  runDB cfg $ do
    P.update uid
      [ Users.LoginDisplayName PSqlite3.=. displayname ]
    void $ P.upsert
      ( UserDetails uid about
      )
      [ UserDetailsAbout PSqlite3.=. about
      ]

-- | Get the user privilege
getPrivilege :: P.Entity Users.Login -> Sql UserPrivilege
getPrivilege (P.Entity uid _) =
  maybe Normal (const Mod) <$> P.getBy (ModId uid)

-- | Set the user privilege
setPrivilege :: UserPrivilege -> Users.LoginId -> Sql ()
setPrivilege priv uid =
  case priv of
    Normal ->
      P.deleteBy (ModId uid)
    Mod ->
      void $ P.insert (Moderator uid)

--

unsafeSetPrivByUsernameIO :: Config -> UserPrivilege -> Users.Username -> IO ()
unsafeSetPrivByUsernameIO cfg priv uname =
  runDB cfg $ do
    muser <- P.getBy (Users.Username uname)
    maybe (error "User not found") (setPrivilege priv . P.entityKey) muser

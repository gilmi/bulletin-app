{- | Common database types and functions

-}


{-# LANGUAGE OverloadedStrings #-}

module Web.Bulletin.Database.Common where

import qualified Database.Persist.Sql as P

import Web.Bulletin.Config
import qualified Web.Scotty.Sqlite.Users as Users


-------------
-- General --
-------------

-- ** General functions

type Sql a = P.SqlPersistT IO a

-- | Execute a sequence of database actions
runDB :: Config -> Sql a -> IO a
runDB cfg dbop = P.runSqlPool dbop (Users.siPool (cfgUsersStateInfo cfg))


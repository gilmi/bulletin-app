{- | User profiles management

-}

{-# language OverloadedStrings #-}
{-# language NamedFieldPuns #-}

module Web.Bulletin.Actions.Profile where


import Control.Monad.IO.Class (liftIO)
import qualified Lucid as H
import qualified Web.Scotty as S
import qualified Database.Persist.Sql as P
import qualified Data.Text.Lazy as TL
import qualified Network.HTTP.Types as HTTP

import qualified Web.Scotty.Sqlite.Users as Users
import qualified Web.Bulletin.Actions.MyUsers as MyUsers
import qualified Web.Bulletin.Database as DB
import Web.Bulletin.Validation
import Web.Bulletin.Model
import Web.Bulletin.Config
import Web.Bulletin.Html

-- * Router

-- | Router for all profile actions
router :: Config -> S.ScottyM ()
router cfg = do
  -- A user profile page
  S.get "/user/:username" $ do
    Users.withMaybeLogin (cfgUsersStateInfo cfg) $ \muser -> do
      name <- S.param "username"
      muser' <- maybe (pure Nothing) (fmap Just . addPriv cfg) muser
      profileGet cfg muser' name

  -- Update a user profile page
  S.post "/user/:username" $ do
    Users.withLogin (cfgUsersStateInfo cfg) $ \user -> do
      username <- S.param "username"
      displayname <- S.param "displayname"
      about <- S.param "about"
      user' <- addPriv cfg user
      profilePost cfg user' username displayname about

  -- Grant moderator privileges to a user
  S.post "/user/:username/mod/grant" $ do
    Users.withMaybeLogin (cfgUsersStateInfo cfg) $ \muser -> do
      name <- S.param "username"
      muser' <- maybe (pure Nothing) (fmap Just . addPriv cfg) muser
      muinfo <- liftIO $ DB.getUserInfoByUsername cfg name
      tryChangeMod cfg Mod muser' name muinfo

  -- Revoke moderator privileges to a user
  S.post "/user/:username/mod/revoke" $ do
    Users.withMaybeLogin (cfgUsersStateInfo cfg) $ \muser -> do
      name <- S.param "username"
      muser' <- maybe (pure Nothing) (fmap Just . addPriv cfg) muser
      muinfo <- liftIO $ DB.getUserInfoByUsername cfg name
      tryChangeMod cfg Normal muser' name muinfo


-- | Get the user privileges of a user and add it in a tuple
addPriv :: Config -> P.Entity Users.Login -> S.ActionM (P.Entity Users.Login, UserPrivilege)
addPriv cfg user =
  (,) user <$> liftIO (DB.runDB cfg (DB.getPrivilege user))

-------------
-- Actions --
-------------

-- * Actions


profileGet :: Config -> Maybe (P.Entity Users.Login, UserPrivilege) -> Users.Username -> S.ActionM ()
profileGet cfg muser queriedUsername = do
  io <- Users.withMaybeLogin (cfgUsersStateInfo cfg) $ MyUsers.loginOrLogout cfg
  mresult <- liftIO $ DB.getUserInfoByUsername cfg queriedUsername
  profileResponse io noProfileErrors muser queriedUsername mresult

profilePost :: Config -> (P.Entity Users.Login, UserPrivilege) -> Users.Username -> Users.Displayname -> TL.Text -> S.ActionM ()
profilePost cfg (P.Entity uid user, priv) username displayname about = do
  if username == Users.loginUsername user
    then do
      muinfo <- liftIO $ DB.getUserInfoByUsername cfg username
      case muinfo of
        Nothing -> do
          S.status HTTP.notFound404
          S.html $
            H.renderText $
              template
                ("Bulletin board - user " <> username <> " not found.")
                ""
                "404 Post not found."
        Just uinfo ->
          submitProfileChanges cfg (uid, priv) uinfo displayname about
    else do
      S.status HTTP.forbidden403
      S.text "403 Access denied."

----

submitProfileChanges :: Config -> (Users.LoginId, UserPrivilege) -> UserInfo -> Users.Displayname -> TL.Text -> S.ActionM ()
submitProfileChanges cfg (uid, priv) userInfo displayname about = do
  io <- Users.withMaybeLogin (cfgUsersStateInfo cfg) $ MyUsers.loginOrLogout cfg
  let
    errs = validateProfile displayname about
  if hasProfileErrors errs
    then
      profileResponse
        io
        errs
        (Just (P.Entity uid (uiLogin userInfo), priv))
        (Users.loginUsername (uiLogin userInfo))
        ( Just $ userInfo
          { uiLogin = (uiLogin userInfo) { Users.loginDisplayName = displayname }
          , uiDetails = (uiDetails userInfo) { userDetailsAbout = about }
          }
        )

    else do
      liftIO $ DB.updateUserInfo cfg uid displayname about
      profileGet cfg (Just (P.Entity uid (uiLogin userInfo), priv)) (Users.loginUsername (uiLogin userInfo))

profileResponse :: Html -> ProfileErrors -> Maybe (P.Entity Users.Login, UserPrivilege) -> Users.Username -> Maybe UserInfo -> S.ActionM ()
profileResponse io errs muser queriedUsername mUserInfo = do
  case (muser, mUserInfo) of
    (_, Nothing) -> do
      S.status HTTP.notFound404
      S.html $
        H.renderText $
          template
            ("Bulletin board - user " <> queriedUsername <> " not found.")
            ""
            "404 User not found."
    (Just (P.Entity _ user, priv), Just userinfo)
      | Users.loginUsername user == queriedUsername ->
        S.html $
          H.renderText $
            template
              ("Bulletin board - @" <> queriedUsername)
              io
              ( myProfileHtml errs userinfo
              )
      | priv == Mod ->
        S.html $
          H.renderText $
            template
              ("Bulletin board - @" <> queriedUsername)
              io
              ( modProfileHtml errs userinfo
              )
    (_, Just userinfo) ->
      S.html $
        H.renderText $
          template
            ("Bulletin board - @" <> queriedUsername)
            io
            ( profileHtml userinfo
            )

tryChangeMod :: Config -> UserPrivilege -> Maybe (P.Entity Users.Login, UserPrivilege) -> Users.Username -> Maybe UserInfo -> S.ActionM ()
tryChangeMod cfg newPriv muser uname uinfo = do
  case (muser, uinfo) of
    (Just (_, priv), _)
      | priv /= Mod -> do
        S.status HTTP.forbidden403
        S.text "403 Access denied."

    (Just (_, Mod), Just userinfo) -> do
      liftIO $ DB.runDB cfg $
        DB.setPrivilege
          newPriv
          (userDetailsLoginId (uiDetails userinfo))
      S.redirect $ "/user/" <> uname

    _ -> do
      S.status HTTP.notFound404
      S.html $
        H.renderText $
          template
            ("Bulletin board - user " <> uname <> " not found.")
            ""
            "404 User not found."

----------
-- Html --
----------

-- * Html

profileHtml :: UserInfo -> Html
profileHtml UserInfo{uiLogin, uiDetails, uiLevel, uiPrivilege} = do
  H.table_ [ H.class_ "profile" ] $ do
    H.tr_ $ do
      H.td_ "Display name:"
      H.td_ $ H.toHtml $ Users.loginDisplayName uiLogin
    H.tr_ $ do
      H.td_ "Username:"
      H.td_ $ usernameLink uiLogin
    H.tr_ $ do
      H.td_ "Level:"
      H.td_ $ H.toHtml $ show uiLevel <> ", " <> ppPriv uiPrivilege
    H.tr_ $ do
      H.td_ "About:"
      H.td_ $ fromMarkdown $ userDetailsAbout uiDetails
    H.tr_ $ do
      H.td_ "Posts:"
      H.td_ $
        H.a_
          [ H.href_ $ TL.toStrict $ "/user/" <> Users.loginUsername uiLogin <> "/posts" ]
          "All posts"
    H.tr_ $ do
      H.td_ "Comments:"
      H.td_ $
        H.a_
          [ H.href_ $ TL.toStrict $ "/user/" <> Users.loginUsername uiLogin <> "/comments" ]
          "All comments"


myProfileHtml :: ProfileErrors -> UserInfo -> Html
myProfileHtml errs UserInfo{uiLogin, uiDetails, uiLevel, uiPrivilege} = do
  H.form_
    [ H.method_ "post"
    , H.action_ $ TL.toStrict $ "/user/" <> Users.loginUsername uiLogin
    , H.onsubmit_ "return confirm('Are you sure you want to save changes to your information?')"
    ] $ do
    H.table_ [ H.class_ "profile" ] $ do
      H.tr_ $ do
        H.td_ "Display name:"
        H.td_ $ do
          applyMaybe (peDisplayName errs) (H.p_ [ H.class_ "error" ] . H.toHtml)
          H.input_ $
            [ H.type_ "text"
            , H.name_ "displayname"
            , H.required_ "true"
            , H.value_ (TL.toStrict $ Users.loginDisplayName uiLogin)
            ]
      H.tr_ $ do
        H.td_ "Username:"
        H.td_ $ usernameLink uiLogin
      H.tr_ $ do
        H.td_ "Password:"
        H.td_ $ H.a_ [ H.href_ "/change-password" ] "Change password"
      H.tr_ $ do
        H.td_ "Level:"
        H.td_ $ H.toHtml $ show uiLevel <> ", " <> ppPriv uiPrivilege
      H.tr_ $ do
        H.td_ "About:"
        H.td_ $ do
          applyMaybe (peAbout errs) (H.p_ [ H.class_ "error" ] . H.toHtml)
          H.textarea_
            [H.name_ "about", H.required_ "true"]
            (H.toHtml $ userDetailsAbout uiDetails)
      H.tr_ $ do
        H.td_ "Invites:"
        H.td_ $ H.a_ [ H.href_ "/invite" ] "Invite tokens"
      H.tr_ $ do
        H.td_ "Posts:"
        H.td_ $
          H.a_
            [ H.href_ $ TL.toStrict $ "/user/" <> Users.loginUsername uiLogin <> "/posts" ]
            "All posts"
      H.tr_ $ do
        H.td_ "Comments:"
        H.td_ $
          H.a_
            [ H.href_ $ TL.toStrict $ "/user/" <> Users.loginUsername uiLogin <> "/comments" ]
            "All comments"

    H.p_ $ H.input_
      [H.type_ "submit"
      , H.value_ "Save Changes"
      , H.class_ "submit-button"
      ]

modProfileHtml :: ProfileErrors -> UserInfo -> Html
modProfileHtml errs UserInfo{uiLogin, uiDetails, uiLevel, uiPrivilege} = do
  let
    (message, button, privact) =
      case uiPrivilege of
        Normal ->
          ("grant moderator privileges to", "Grant moderator privileges", "grant")
        Mod ->
          ("revoke moderator privileges from", "Revoke moderator privileges", "revoke")
  H.div_ $ H.form_
    [ H.method_ "post"
    , H.action_ $ TL.toStrict $ "/user/" <> Users.loginUsername uiLogin <> "/mod/" <> privact
    , H.onsubmit_ $ "return confirm('Are you sure you want to " <> message <> " this user?')"
    , H.class_ "grant-revoke-mod"
    ]
    ( H.input_
      [ H.type_ "submit"
      , H.value_ button
      , H.class_ "submit-button"
      ]
    )

  H.form_
    [ H.method_ "post"
    , H.action_ $ TL.toStrict $ "/user/" <> Users.loginUsername uiLogin
    , H.onsubmit_ "return confirm('Are you sure you want to save changes to your information?')"
    ] $ do
    H.table_ [ H.class_ "profile" ] $ do
      H.tr_ $ do
        H.td_ "Display name:"
        H.td_ $ do
          applyMaybe (peDisplayName errs) (H.p_ [ H.class_ "error" ] . H.toHtml)
          H.input_ $
            [ H.type_ "text"
            , H.name_ "displayname"
            , H.required_ "true"
            , H.value_ (TL.toStrict $ Users.loginDisplayName uiLogin)
            ]
      H.tr_ $ do
        H.td_ "Username:"
        H.td_ $ usernameLink uiLogin
      H.tr_ $ do
        H.td_ "Password:"
        H.td_ $ H.a_ [ H.href_ "/change-password" ] "Change password"
      H.tr_ $ do
        H.td_ "Level:"
        H.td_ $ H.toHtml $ show uiLevel <> ", " <> ppPriv uiPrivilege
      H.tr_ $ do
        H.td_ "About:"
        H.td_ $ do
          applyMaybe (peAbout errs) (H.p_ [ H.class_ "error" ] . H.toHtml)
          H.textarea_
            [H.name_ "about"]
            (H.toHtml $ userDetailsAbout uiDetails)
      H.tr_ $ do
        H.td_ "Invites:"
        H.td_ $ H.a_ [ H.href_ "/invite" ] "Invite tokens"
      H.tr_ $ do
        H.td_ "Posts:"
        H.td_ $
          H.a_
            [ H.href_ $ TL.toStrict $ "/user/" <> Users.loginUsername uiLogin <> "/posts" ]
            "All posts"
      H.tr_ $ do
        H.td_ "Comments:"
        H.td_ $
          H.a_
            [ H.href_ $ TL.toStrict $ "/user/" <> Users.loginUsername uiLogin <> "/comments" ]
            "All comments"

    H.p_ $ H.input_
      [ H.type_ "submit"
      , H.value_ "Save Changes"
      , H.class_ "submit-button"
      ]


----------------
-- Validation --
----------------

-- * Validation

data ProfileErrors
  = ProfileErrors
    { peDisplayName :: Maybe TL.Text
    , peAbout :: Maybe TL.Text
    }
    deriving (Show, Eq)

noProfileErrors :: ProfileErrors
noProfileErrors = ProfileErrors Nothing Nothing

hasProfileErrors :: ProfileErrors -> Bool
hasProfileErrors errs =
  errs /= noProfileErrors

validateProfile :: Users.Displayname -> TL.Text -> ProfileErrors
validateProfile displayname about =
  ProfileErrors
    { peDisplayName =
      if TL.length displayname < 2 || 40 < TL.length displayname
        then
          Just $ TL.unwords
            [ "A display name must be at least 2 characters long and at most 40 characters long."
            , "Current length is: " <> TL.pack (show (TL.length displayname))
            ]
        else
          Nothing
    , peAbout =
      if TL.length about > 300
        then
          Just $ TL.unwords
            [ "About must be at most 300 characters long."
            , "Current length is: " <> TL.pack (show (TL.length about))
            ]
        else
          Nothing
    }

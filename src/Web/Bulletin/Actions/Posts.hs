{- | Post management

-}

{-# language OverloadedStrings #-}

module Web.Bulletin.Actions.Posts where


import Data.Maybe (isJust)
import Control.Monad
import Control.Monad.IO.Class (liftIO)
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import qualified Lucid as H
import qualified Web.Scotty as S
import qualified Database.Persist.Sql as P
import qualified Data.Time as C
import qualified Network.HTTP.Types as HTTP

import qualified Web.Scotty.Sqlite.Users as Users
import qualified Web.Bulletin.Actions.MyUsers as MyUsers
import qualified Web.Bulletin.Database as DB
import Web.Bulletin.Config
import Web.Bulletin.Html
import Web.Bulletin.Model
import Web.Bulletin.Validation

-------------
-- Routing --
-------------

-- * Router

-- | Router for all post actions
router :: Config -> S.ScottyM ()
router cfg = do
  -- A page for a specific post
  S.get "/post/id/:id" $ do
    Users.withMaybeLogin (cfgUsersStateInfo cfg) $ \muid -> do
      pid <- P.toSqlKey <$> S.param "id"
      let
        newcommenthtml = case muid of
          Nothing -> ""
          Just{} -> newCommentHtml (CNew pid) noNewCommentErrors ""
      displayPostWithComments cfg pid newcommenthtml

  -- A page for creating a new post
  S.get "/post/new" $
    Users.withLogin (cfgUsersStateInfo cfg) $ \_ ->
      servePostForm cfg New noNewPostErrors "" ""

  -- A request to submit a new page
  S.post "/post/new" $
    Users.withLogin (cfgUsersStateInfo cfg) $ \user -> do
      title <- S.param "title"
      content <- S.param "content"
      submitNewPostForm cfg title content (Users.getLoginId user)

  -- serve edit post form
  S.get "/post/id/:id/edit" $
    Users.withLogin (cfgUsersStateInfo cfg) $ \_ -> do
      pid <- P.toSqlKey <$> S.param "id"
      mpost <- liftIO $ DB.runDB cfg $ DB.getPostBrief pid
      case mpost of
        Nothing -> do
          S.status HTTP.notFound404
          S.text "404 Not Found."
        Just (P.Entity _ post, _, _) ->
          servePostForm cfg (Edit pid) noNewPostErrors (postTitle post) (postContent post)

  -- A request to submit a editing of a page
  S.post "/post/id/:id/edit" $
    Users.withLogin (cfgUsersStateInfo cfg) $ \user -> do
      pid <- P.toSqlKey <$> S.param "id"
      title <- S.param "title"
      content <- S.param "content"
      submitEditPostForm cfg user pid title content

  -- A request to delete a specific post
  S.post "/post/id/:id/delete" $
    Users.withLogin (cfgUsersStateInfo cfg) $ \user -> do
      pid <- P.toSqlKey <$> S.param "id"
      deletePost cfg user pid

  -- A request to lock a specific post
  S.post "/post/id/:id/lock" $
    Users.withLogin (cfgUsersStateInfo cfg) $ \user -> do
      pid <- P.toSqlKey <$> S.param "id"
      lockPost cfg user pid True

  -- A request to unlock a specific post
  S.post "/post/id/:id/unlock" $
    Users.withLogin (cfgUsersStateInfo cfg) $ \user -> do
      pid <- P.toSqlKey <$> S.param "id"
      lockPost cfg user pid False

  -- A request to hide a specific post
  S.post "/post/id/:id/hide" $
    Users.withLogin (cfgUsersStateInfo cfg) $ \user -> do
      pid <- P.toSqlKey <$> S.param "id"
      hidePost cfg user pid True

  -- A request to unhide a specific post
  S.post "/post/id/:id/unhide" $
    Users.withLogin (cfgUsersStateInfo cfg) $ \user -> do
      pid <- P.toSqlKey <$> S.param "id"
      hidePost cfg user pid False

  -- A request to submit a new comment
  S.post "/post/id/:id/comment" $
    Users.withLogin (cfgUsersStateInfo cfg) $ \user -> do
      pid <- P.toSqlKey <$> S.param "id"
      content <- S.param "content"
      submitNewCommentForm cfg pid content (Users.getLoginId user)

  -- User posts
  S.get "/user/:name/posts" $ do
    name <- S.param "name"
    userPosts cfg name

  -- User posts
  S.get "/user/:name/comments" $ do
    name <- S.param "name"
    userComments cfg name


data Route
  = New
  | Edit PostId
    deriving Eq

-----------
-- Posts --
-----------

-- * Posts

-- ** Actions

displayAllPosts :: Config -> S.ActionM Html
displayAllPosts cfg = do
  minfo <- maybe (pure Nothing) (liftIO . DB.getUserInfoById cfg) =<< Users.getSession (cfgUsersStateInfo cfg)
  posts <- liftIO $ DB.getAllPosts cfg
  pure $ do
    when (isJust minfo) $ do
      H.p_ [ H.class_ "new-button" ] $ H.a_ [H.href_ "/post/new"] "New Post"
    H.div_ [ H.class_ "posts" ] $
      mapM_ (postHtml False minfo) posts

userPosts :: Config -> Users.Username -> S.ActionM ()
userPosts cfg queriedUsername = do
  io <- Users.withMaybeLogin (cfgUsersStateInfo cfg) $ MyUsers.loginOrLogout cfg
  posts <- liftIO $ DB.getAuthorPostsByUsername cfg queriedUsername
  S.html $
    H.renderText $
      template
        ("Bulletin board - user " <> queriedUsername)
        io
        ( do
          H.h2_ $ "Posts made by " <> usernameTextLink queriedUsername
          H.div_ [ H.class_ "posts" ] $
            mapM_ (postHtml False Nothing) posts
        )

userComments :: Config -> Users.Username -> S.ActionM ()
userComments cfg queriedUsername = do
  io <- Users.withMaybeLogin (cfgUsersStateInfo cfg) $ MyUsers.loginOrLogout cfg
  comms <- liftIO $ DB.getAuthorCommentsByUsername cfg queriedUsername
  S.html $
    H.renderText $
      template
        ("Bulletin board - user " <> queriedUsername)
        io
        ( do
          H.h2_ $ "Comments made by " <> usernameTextLink queriedUsername
          H.div_ [ H.class_ "comments" ] $
            forM_ comms $ \(pid, title, P.Entity _ comment) -> do
              H.div_ [ H.class_ "post" ] $ do
                H.div_ [ H.class_ "post-header" ] $ do
                  H.p_ [ H.class_ "post-title" ] $ "On " <>
                    H.a_
                      [H.href_ (TL.toStrict $ "/post/id/" <> showPostKey pid)]
                      (H.toHtml title)

                  H.div_ [ H.class_ "comment" ] $ do
                    H.p_ [ H.class_ "comment-author" ] $
                      H.toHtml queriedUsername <> ":"
                    H.div_ [ H.class_ "comment-content" ] $
                      fromMarkdown (commentContent comment)
        )



displayPostWithComments :: Config -> PostId -> Html -> S.ActionM ()
displayPostWithComments cfg pid newcommenthtml = do
  mpost <- liftIO $ DB.runDB cfg $ DB.getPostWithComments pid
  minfo <- maybe (pure Nothing) (liftIO . DB.getUserInfoById cfg) =<< Users.getSession (cfgUsersStateInfo cfg)
  io <- Users.withMaybeLogin (cfgUsersStateInfo cfg) $ MyUsers.loginOrLogout cfg
  case mpost of
    Just (pb@(post, _, _), comments) ->
      S.html $
        H.renderText $
          template
            ("Bulletin board - post " <> TL.pack (show pid))
            io
            ( do
              postHtml True minfo pb
              commentsHtml comments
              when (not $ postLocked $ P.entityVal post) $
                newcommenthtml
            )

    Nothing -> do
      S.status HTTP.notFound404
      S.html $
        H.renderText $
          template
            ("Bulletin board - post " <> TL.pack (show pid) <> " not found.")
            ""
            "404 Post not found."

servePostForm :: Config -> Route -> NewPostErrors -> TL.Text -> TL.Text -> S.ActionM ()
servePostForm cfg route errs title content = do
  io <- Users.withMaybeLogin (cfgUsersStateInfo cfg) $ MyUsers.loginOrLogout cfg
  S.html $
    H.renderText $
      template
        ("Bulletin board - add new post")
        io
        (newPostHtml route errs title content)

submitNewPostForm :: Config -> TL.Text -> TL.Text -> Users.LoginId -> S.ActionM ()
submitNewPostForm cfg title content uid = do
  let
    errs = validateNewPost title content
  if hasNewPostErrors errs
    then do
      servePostForm cfg New errs title content
    else do
      time <- liftIO C.getCurrentTime
      pid <- liftIO $ DB.insertPost cfg
        ( Post
          { postDate = time
          , postAuthorId = uid
          , postTitle = title
          , postContent = content
          , postLocked = False
          , postHidden = False
          }
        )
      S.redirect ("/post/id/" <> showPostKey pid)

submitEditPostForm :: Config -> P.Entity Users.Login -> PostId -> TL.Text -> TL.Text -> S.ActionM ()
submitEditPostForm cfg user pid title content = do
  let
    errs = validateNewPost title content
  if hasNewPostErrors errs
    then do
      servePostForm cfg (Edit pid) errs title content
    else do
      status <- liftIO $ DB.editPost cfg user pid title content
      case status of
        DB.Success ->
          S.redirect ("/post/id/" <> showPostKey pid)
        DB.FailNotAllowed -> do
          S.status HTTP.forbidden403
          S.text "You do not have permissions to edit this post."
        DB.FailDoesn'tExist -> do
          S.status HTTP.notFound404
          S.text "404 Not Found."

deletePost :: Config -> P.Entity Users.Login -> PostId -> S.ActionM ()
deletePost cfg user pid = do
  editPostAction "/" $ DB.deletePost cfg user pid

lockPost :: Config -> P.Entity Users.Login -> PostId -> Bool -> S.ActionM ()
lockPost cfg user pid lock = do
  editPostAction ("/post/id/" <> showPostKey pid) $ DB.lockPost cfg user pid lock

hidePost :: Config -> P.Entity Users.Login -> PostId -> Bool -> S.ActionM ()
hidePost cfg user pid hide = do
  editPostAction ("/post/id/" <> showPostKey pid) $ DB.hidePost cfg user pid hide

editPostAction :: TL.Text -> IO DB.EditPostStatus -> S.ActionM ()
editPostAction route dbaction = do
  status <- liftIO dbaction
  case status of
    DB.Success ->
      S.redirect route
    DB.FailNotAllowed -> do
      S.status HTTP.forbidden403
      S.text "You do not have permissions to edit this post."
    DB.FailDoesn'tExist -> do
      S.status HTTP.notFound404
      S.text "404 Not Found."

----------
-- Html --
----------

-- ** Html

postHtml :: Bool -> Maybe UserInfo -> PostBrief -> Html
postHtml displayEdit muser (P.Entity pidKey post, P.Entity _ user, commentsAmount) = do
  H.div_ [ H.class_ "post" ] $ do
    when
      ( displayEdit &&
        ( fmap (userDetailsLoginId . uiDetails) muser == Just (postAuthorId post) && not (postLocked post)
          || fmap uiPrivilege muser == Just Mod
        )
      ) $ do
      H.section_ [ H.class_ "edit-post-section" ] $ do
        when (fmap uiPrivilege muser == Just Mod)
          ( do
            let
              lock
                | postLocked post =
                    ("Unlock", "🔓", "unlock")
                | otherwise =
                    ("Lock", "🔒", "lock")
              hide
                | postHidden post =
                    ("Unhide", "👁️", "unhide")
                | otherwise =
                    ("Hide", "👁️", "hide")
              modform :: (T.Text, T.Text, TL.Text) -> Html
              modform (message, button, route) =
                H.form_
                  [ H.method_ "post"
                  , H.action_ (TL.toStrict $ "/post/id/" <> showPostKey pidKey <> "/" <> route)
                  , H.onsubmit_ $ "return confirm('Are you sure you want to " <> T.toLower message <> " this post?')"
                  , H.class_ "lock-post"
                  ]
                  (H.input_ [H.type_ "submit", H.class_ "lockbtn", H.value_ button, H.title_ message])
            modform hide
            modform lock
          )
        H.form_
          [ H.method_ "get"
          , H.action_ (TL.toStrict $ "/post/id/" <> showPostKey pidKey <> "/edit")
          , H.class_ "edit-post"
          ]
          ( do
            H.input_ [H.type_ "submit", H.value_ "E", H.class_ "editbtn", H.title_ "Edit"]
          )
        H.form_
          [ H.method_ "post"
          , H.action_ (TL.toStrict $ "/post/id/" <> showPostKey pidKey <> "/delete")
          , H.onsubmit_ "return confirm('Are you sure you want to delete this post?')"
          , H.class_ "delete-post"
          ]
          ( do
            H.input_ [H.type_ "submit", H.value_ "X", H.class_ "deletebtn", H.title_ "Delete"]
          )

    when (postLocked post) $ do
      H.section_ [ H.class_ "edit-post-section" ] $ do
        H.span_ [H.title_ "Locked"] "🔒" -- lock emoji

    H.div_ [ H.class_ "post-header" ] $ do
      H.h2_ [ H.class_ "post-title" ] $
        H.a_
          [H.href_ (TL.toStrict $ "/post/id/" <> showPostKey pidKey)]
          (H.toHtml $ postTitle post)

      H.div_ $ do
        "by "
        H.span_ [ H.class_ "post-author" ] $ displaynameLink user
        " on "
        H.span_ [ H.class_ "post-time" ] $ ppDate (postDate post)

    H.div_ [H.class_ "post-content"] $ do
      fromMarkdown $ postContent post

    H.div_ $
      H.a_ [H.href_ (TL.toStrict $ "/post/id/" <> showPostKey pidKey <> "#comments")] $
        commentsAmountMsg commentsAmount

commentsAmountMsg :: Int -> Html
commentsAmountMsg n
  | n == 1 = "1 comment"
  | otherwise = H.toHtml $ show n <> " comments"

newPostHtml :: Route -> NewPostErrors -> TL.Text -> TL.Text -> Html
newPostHtml route errs title content = do
  H.form_
    ( [ H.method_ "post"
      , H.action_ $ case route of
        New -> "/post/new"
        Edit pid -> TL.toStrict $ "/post/id/" <> showPostKey pid <> "/edit"
      , H.class_ "new-post"
      ] <> [ H.onsubmit_ "return confirm('Are you sure you want to update this post?')" | route /= New ]
    )
    ( do
      applyMaybe (npeTitle errs) (H.p_ [ H.class_ "error" ] . H.toHtml)
      H.p_ $ H.input_ $
        [ H.type_ "text"
        , H.name_ "title"
        , H.required_ "true"
        , H.placeholder_ "Title..."
        , H.value_ (TL.toStrict title)
        ] <> newPostFocus Title errs

      applyMaybe (npeContent errs) (H.p_ [ H.class_ "error" ] . H.toHtml)
      H.p_ $ H.textarea_
        ([H.name_ "content", H.required_ "true", H.placeholder_ "Content..."] <> newPostFocus Content errs)
        (H.toHtml content)

      let
        btntext = case route of
          New -> "Create"
          Edit{} -> "Update"
      H.p_ $ H.input_ [H.type_ "submit", H.value_ btntext, H.class_ "submit-button"]
    )


newPostFocus :: ErrorPart -> NewPostErrors -> [H.Attribute]
newPostFocus part errs =
  case part of
    Title
      | not (hasNewPostErrors errs) || isJust (npeTitle errs) ->
        [H.autofocus_]
    Content
      | not (isJust (npeTitle errs)) && isJust (npeContent errs) ->
        [H.autofocus_]
    _ -> []

ppDate :: C.UTCTime -> Html
ppDate =
  H.toHtml . C.formatTime C.defaultTimeLocale "%Y-%m-%d %H:%M:%S %z"

----------------
-- Validation --
----------------

-- ** Validation

data ErrorPart
  = Title
  | Content

data NewPostErrors
  = NewPostErrors
    { npeTitle :: Maybe TL.Text
    , npeContent :: Maybe TL.Text
    }
    deriving (Show, Eq)

noNewPostErrors :: NewPostErrors
noNewPostErrors = NewPostErrors Nothing Nothing

hasNewPostErrors :: NewPostErrors -> Bool
hasNewPostErrors errs =
  errs /= noNewPostErrors

validateNewPost :: TL.Text -> TL.Text -> NewPostErrors
validateNewPost title content =
  NewPostErrors
    { npeTitle =
      if TL.length title < 1 || TL.length title > 200
        then
          Just $ TL.unwords
            [ "Title must be at least 1 character long and at most 200 characters long."
            , "Current length is: " <> TL.pack (show (TL.length title))
            ]
        else
          Nothing
    , npeContent =
      if TL.length content < 10 || 2000 < TL.length content
        then
          Just $ TL.unwords
            [ "Content must be at least 10 characters long and at most 2000 characters long."
            , "Current length is: " <> TL.pack (show (TL.length content))
            ]
        else
          Nothing
    }

-----------------------------------------

--------------
-- Comments --
--------------

-- * Comments

data CRoute
  = CNew PostId

-------------
-- Actions --
-------------

-- ** Comments Actions

submitNewCommentForm :: Config -> PostId -> TL.Text -> Users.LoginId -> S.ActionM ()
submitNewCommentForm cfg pid content uid = do
  let
    errs = validateNewComment content
  if hasNewCommentErrors errs
    then do
      displayPostWithComments cfg pid $ newCommentHtml (CNew pid) errs content
    else do
      time <- liftIO C.getCurrentTime
      _ <- liftIO $ DB.insertComment cfg
        ( Comment
          { commentDate = time
          , commentAuthorId = uid
          , commentPostId = pid
          , commentContent = content
          }
        )
      S.redirect ("/post/id/" <> showPostKey pid)

----------
-- Html --
----------

-- ** Comments Html

commentsHtml :: Comments -> Html
commentsHtml comments = do
  H.div_ [ H.id_ "comments", H.class_ "comments" ] $ do
    H.h3_ "Comments"
    mapM_ commentHtml comments

commentHtml :: (Comment', User') -> Html
commentHtml (P.Entity _ comment, P.Entity _ user) = do
  H.div_ [ H.class_ "comment" ] $ do
    H.p_ [ H.class_ "comment-author" ] $ displaynameLink user <> ":"
    H.div_ [ H.class_ "comment-content" ] $
      fromMarkdown (commentContent comment)


newCommentHtml :: CRoute -> NewCommentErrors -> TL.Text -> Html
newCommentHtml route errs content = do
  H.form_
    [ H.method_ "post"
    , H.action_ $ case route of
      CNew pidKey -> TL.toStrict $ "/post/id/" <> showPostKey pidKey <> "/comment"
    , H.class_ "new-comment"
    ]
    ( do
      applyMaybe (nceContent errs) (H.p_ [ H.class_ "error" ] . H.toHtml)
      H.p_ $ H.textarea_
        [H.name_ "content", H.required_ "true", H.placeholder_ "Comment...", H.autofocus_]
        (H.toHtml content)

      let
        btntext = case route of
          CNew{} -> "Comment"
      H.p_ $ H.input_ [H.type_ "submit", H.value_ btntext, H.class_ "submit-button"]
    )


----------------
-- Validation --
----------------

-- ** Comments Validation

data NewCommentErrors
  = NewCommentErrors
    { nceContent :: Maybe TL.Text
    }
    deriving (Show, Eq)

noNewCommentErrors :: NewCommentErrors
noNewCommentErrors = NewCommentErrors Nothing

hasNewCommentErrors :: NewCommentErrors -> Bool
hasNewCommentErrors errs =
  errs /= noNewCommentErrors

validateNewComment :: TL.Text -> NewCommentErrors
validateNewComment content =
  NewCommentErrors
    { nceContent =
      if TL.length content < 10 || 2000 < TL.length content
        then
          Just $ TL.unwords
            [ "A comment must be at least 10 characters long and at most 2000 characters long."
            , "Current length is: " <> TL.pack (show (TL.length content))
            ]
        else
          Nothing
    }

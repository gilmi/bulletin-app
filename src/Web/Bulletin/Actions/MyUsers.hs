{- | User management
-}


{-# language OverloadedStrings #-}

module Web.Bulletin.Actions.MyUsers where


import Control.Monad (when)
import Data.List (intersperse)
import Control.Monad.IO.Class (liftIO)
import qualified Lucid as H
import qualified Web.Scotty as S
import qualified Database.Persist.Sql as P
import qualified Data.Text as T

import qualified Web.Scotty.Sqlite.Users as Users
import qualified Web.Scotty.Sqlite.Users.NewUser as NewUser
import qualified Web.Bulletin.Database as DB
import Web.Bulletin.Config
import Web.Bulletin.Html

-- * Router

-- | Router for all user actions
router :: Config -> S.ScottyM ()
router cfg = do
  
  when (cfgRegistration cfg == OpenRegistration) $
    NewUser.router (cfgUsersStateInfo cfg) mytemplate

  -- A page for registering a new user
  S.get "/register/:token" $ do
    token <- S.param "token"
    myRegisterGet cfg token

  -- A request to create a new user
  S.post "/register/:token" $ do
    token <- S.param "token"
    myRegisterPost cfg token

  -- A page for generating registration tokens for new users
  S.get "/invite" $
    Users.withLogin (cfgUsersStateInfo cfg) $ \user -> do
      myInvite cfg user

-------------
-- Actions --
-------------

-- * Actions

-- | Returns html to welcome the user, providing links to register/login or logout.
loginOrLogout :: Config -> Maybe (P.Entity Users.Login) -> S.ActionM Html
loginOrLogout cfg muser = do
  pure $
    H.div_ [ H.class_ "welcome" ] $
      case fmap P.entityVal muser of
        Just user ->
          mconcat
            [ "Welcome "
            , displaynameLink user
            , "! "
            , "["
            , mconcat $ intersperse " |"
              [ H.form_ [H.method_ "post" , H.action_ "/logout", H.class_ "logout-form"] $
                H.input_ [H.type_ "submit", H.value_ "Log out", H.class_ "logout"]
              ]
            , "]"
            ]
        Nothing ->
          mconcat
            [ "[ "
            , mconcat $ intersperse " | " $
              ( if cfgRegistration cfg == OpenRegistration
                then (:) (H.a_ [ H.href_ "/register" ] "Register")
                else id
              )
              [ H.a_ [ H.href_ "/login" ] "Log in"
              ]
            , " ]"
            ]

myRegisterGet :: Config -> T.Text -> S.ActionM ()
myRegisterGet cfg token = do
    isvalid <- liftIO $ DB.isValidToken cfg token
    if isvalid
      then do
        result <- NewUser.registerGetAction (cfgUsersStateInfo cfg) mytemplate ("/register/" <> token)
        NewUser.runFinalAction result
      else do
        S.html $ H.renderText $ mytemplate "Not found" "Invalid registration token"

myRegisterPost :: Config -> T.Text -> S.ActionM ()
myRegisterPost cfg token = do
    isvalid <- liftIO $ DB.isValidToken cfg token
    if isvalid
      then do
        result <- NewUser.registerPostAction (cfgUsersStateInfo cfg) mytemplate ("/register/" <> token)
        case result of
          NewUser.RegistrationSuccess{} ->
            liftIO $ DB.deleteToken cfg token
          _ ->
            pure ()
        NewUser.runFinalAction result
      else do
        S.html $ H.renderText $ template "Not found" "" ""

myInvite :: Config -> P.Entity Users.Login -> S.ActionM ()
myInvite cfg user@(P.Entity uid _) = do
  tokens <- liftIO $ DB.getTokensByUserId cfg uid
  let
    size = length tokens
    amount
      | size == 0 = "no tokens"
      | size == 1 = "1 token"
      | otherwise = show size <> " tokens"
  welcome <- loginOrLogout cfg (Just user)
  S.html $
    H.renderText $
      template
        "Bulletin Board - Invite a new user"
        welcome
        $ do
          H.p_ $ H.toHtml $ "You have " <> amount <> " available."
          if size > 0
            then do
              H.p_ "Send one of these links to your friend to invite them:"
              H.ul_ $
                mapM_ (\token -> H.li_ $ H.a_ [ H.href_ $ "/register/" <> token ] "Invite link") tokens
            else ""
  


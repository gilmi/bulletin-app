-- | CSS style as a raw string

{-# language QuasiQuotes #-}

module Web.Bulletin.Style where

import Text.RawString.QQ
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.Encoding as TL
import qualified Data.ByteString.Lazy as BSL


-- CSS --

style :: BSL.ByteString
style = TL.encodeUtf8 $ TL.pack [r|
html {
  min-width: 500px;
}

body {
  margin-top: 40px;
   color: #333;
// color: #CCC;
// background-color: #333;
  font-family: monospace;
  font-size: 1.3em;
}

.main {
  max-width: 900px;
  min-width: 500px;
  box-sizing: border-box;
  margin: auto;
}

a {
  color: #fb7a91;
}

a:hover {
  color: #ff2c52;
}

.head {
  max-width: 900px;
  min-width: 500px;
  margin: auto;
  box-sizing: border-box;
  margin: auto;
}

ul {
  padding-left: 0;
  margin: 0;
}

li {
  padding-bottom: 0.7em;
  list-style-position:inside;
  overflow: hidden;
  text-overflow: "...";
}

.error {
  color: #f44336;
  font-size: 0.7em;
}

input[type=text], input[type=url] {
  min-width: 300px;
}
input[type=password], input[type=url] {
  min-width: 300px;
}
input[type=submit] {
}
input {
  font-family: monospace;
}

input::placeholder, textarea::placeholder {
}

textarea {
  overflow-y: scroll;
  min-height: 200px;
  width: 60%;
  min-width: 460px;
  box-sizing: border-box;
}

.header-h1 {
  display: inline-block;
  margin: 20px 20px;
  margin-right: 0;
}
.header- {
  display: inline-block;
}
.header {
  color: #86c6ff;
  text-decoration: none;
}
.header:hover {
  color: #4aaaff;
}

h4 {
  color: #fdfd95;
  margin: 0;
}

.edit-post-section {
  float: right;
  clear: both;
}

.edit-post, .delete-post, .lock-post {
  display: inline-block;
}
.deletebtn, .editbtn, .lockbtn {
  cursor: pointer;
  background-color: #f44336;
  border: none;
  color: white;
  padding-left: 0.3em;
  padding-right: 0.3em;
  padding-top: 0.1em;
  padding-bottom: 0.1em;
  text-align: center;
  font-weight: bold;
  text-decoration: none;
  display: inline-block;
  font-family: monospace;
  font-size: 1.1em;
  width: 1.5em !important;
  height: 1.5em;
}
.deletebtn:hover {
  background-color: #b00;
}
.editbtn {
  background-color: #fbbb40;
}
.editbtn:hover {
  background-color: #bf7e00;
}
.lockbtn {
  background-color: initial;
}
.lockbtn:hover {
  background-color: initial;
}


.date {
  font-size: 0.7em;
  margin-right: 0.5em;
}

.tag {
  color: #c0f3ff;
  text-decoration: none;
  font-size: 0.7em;
}

.tag:hover {
  color: #73dff9;
  text-decoration: underline;
  font-size: 0.7em;
}

.comma {
  margin-right: 0.5em;
  font-size: 0.7em;
}

.tags {
  max-width: 98%;
  margin: 20px auto;
  text-align: center;
}

h2 {
  margin-top: 0px;
}

.posts {
  min-width: 400px;
  margin: 20px auto;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
}

.post {
  display: inline-block;
  margin-right: 0.5em;
  margin-bottom: 0.5em;
  padding: 0.8em;
  border: #666 1px dashed;
  flex-grow: 1;
  // max-width: 45%;
  min-width: 20em;
}

.post-author {
  font-weight: bold;
}

.comment {
  display: flex;
}

.comment-author {
  font-weight: bold;
  margin-right: 1em;
}

.new-comment textarea {
  overflow-y: scroll;
  min-height: 100px;
  width: 40%;
  min-width: 460px;
  box-sizing: border-box;
}

.logo {
  display: inline-block;
}
.welcome {
  display: inline-block;
  float: right;
  clear: both;
  font-size: 0.9em;
  margin-top: 37px;
  margin-bottom: 37px;
}
.welcome input {
  font-size: 0.9em;
}

.logout-form {
  display: inline-block;
}
.logout {
  display: inline-block;
  width: initial !important;
  background-color: transparent;
  text-decoration: underline;
  border: none;
  cursor: pointer;
  color: #fb7a91;
}

.logout:hover {
  color: #ff2c52;
}
.logout:focus {
  outline: none;
  color: #fb7a91;
}

.username-link {
  text-decoration: none;
}
.username-link:hover {
  text-decoration: underline;
}

.displayname-link {
  color: #9d1818;
  text-decoration: none;
}
.displayname-link:hover {
  text-decoration: underline;
  color: #df1b1b;
}

td {
  min-width: 150px;
  vertical-align: top;
}
table {
  border-spacing: 0.5em;
}
td p {
  margin-top: 0px;
}
|]

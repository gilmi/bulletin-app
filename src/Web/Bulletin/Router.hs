{- | Website router.

-}

{-# language OverloadedStrings #-}

module Web.Bulletin.Router where

import Control.Monad (when)
import qualified Web.Scotty as S
import qualified Lucid as H

import Web.Bulletin.Config
import Web.Bulletin.Style
import Web.Bulletin.Html

import qualified Web.Scotty.Sqlite.Users as Users
import qualified Web.Scotty.Sqlite.Users.Login as Login
import qualified Web.Scotty.Sqlite.Users.ChangePassword as ChangePassword

import qualified Web.Bulletin.Actions.Posts as Posts
import qualified Web.Bulletin.Actions.MyUsers as MyUsers
import qualified Web.Bulletin.Actions.Profile as Profile

-------------
-- Routing --
-------------

router :: Config -> S.ScottyM ()
router cfg = do
  -- We want login, user management and styling available always
  Login.router (cfgUsersStateInfo cfg) mytemplate
  MyUsers.router cfg

  -- css styling
  S.get "/style.css" $ do
    S.setHeader "Content-Type" "text/css; charset=utf-8"
    S.raw style

  -- We'll catch all further traffic and redirect
  -- when the site is private and the user is not logged in
  when (cfgVisibility cfg == Private) $ do
    S.get (S.regex ".*") $ Users.withMaybeLogin (cfgUsersStateInfo cfg) $ \mlogin ->
      case mlogin of
        Nothing ->
          S.redirect "/login"
        Just{} ->
          S.next

  S.get "/" $ index cfg
  ChangePassword.router (cfgUsersStateInfo cfg) mytemplate
  Profile.router cfg
  Posts.router cfg


index :: Config -> S.ActionM ()
index cfg = do
  posts <- Posts.displayAllPosts cfg
  io <- Users.withMaybeLogin (cfgUsersStateInfo cfg) $ MyUsers.loginOrLogout cfg
  S.html $
    H.renderText $
      template
        ("Bulletin board")
        io
        posts

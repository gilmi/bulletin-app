{- | Common HTML functions
-}


{-# language OverloadedStrings #-}

module Web.Bulletin.Html where

import qualified Data.Text.Lazy as TL
import qualified Lucid as H
import qualified Cheapskate.Lucid as MD
import qualified Cheapskate as MD (markdown, Options(..))
import qualified Web.Scotty.Sqlite.Users as Users


----------
-- HTML --
----------

-- | Lucid Html type
type Html = H.Html ()

-- | template for @my-scotty-users@ pages
mytemplate :: Users.HtmlTemplate
mytemplate title = template ("Bulletin Board - " <> title) ""

-- | Basic website template including html, charset, stylesheet, etc.
template :: TL.Text -> Html -> Html -> Html
template title header content =
  H.doctypehtml_ $ do
    H.head_ $ do
      H.meta_ [ H.charset_ "utf-8" ]
      H.title_ (H.toHtml title)
      H.link_ [ H.rel_ "stylesheet", H.type_ "text/css", H.href_ "/style.css"  ]
    H.body_ $ do
      H.div_ [ H.class_ "head" ] $ do
        H.h1_ [ H.class_ "logo" ] $
          H.a_ [H.href_ "/"] "Bulletin Board"
        header
      H.div_ [ H.class_ "main" ] $ do
        content


-- | Convert a markdown text to HTML
fromMarkdown :: TL.Text -> Html
fromMarkdown =
  MD.renderDoc . MD.markdown markdownOptions . TL.toStrict . TL.filter (/='\r')

markdownOptions :: MD.Options
markdownOptions =
  MD.Options
    { MD.sanitize = True
    , MD.allowRawHtml = False
    , MD.preserveHardBreaks = True
    , MD.debug = False
    }

-- | Convert a username to a profile link
usernameTextLink :: TL.Text -> Html
usernameTextLink username =
  H.a_
    [ H.href_ $ TL.toStrict $ "/user/" <> username
    , H.class_ "username-link"
    ]
    (H.toHtml $ "@" <> username)

-- | Convert a @Login@ to a profile link
usernameLink :: Users.Login -> Html
usernameLink = usernameTextLink . Users.loginUsername

-- | Convert a display name to a profile link
displaynameLink :: Users.Login -> Html
displaynameLink login =
  H.a_
    [ H.href_ $ TL.toStrict $ "/user/" <> Users.loginUsername login
    , H.class_ "displayname-link"
    ]
    (H.toHtml $ Users.loginDisplayName login)

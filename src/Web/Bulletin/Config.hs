{- | Cofiguration

The following settings are available using environment variables:

* @SCOTTY_ENV@: server environment, three options:
    * @Development@ - One major thing to note here is that cookies are not flagged as @secure@ so it can be used without TLS
    * @Production@ - Cookies are marked as secure so can only really work with TLS.
    * @Testing@ - Not really in use at the moment, sorry for being lazy and not writing tests.
* @CONN_STRING@ - sqlite3 connection string ([see URI filename examples](https://www.sqlite.org/c3ref/open.html)).
* @PORT@
* @VISIBLE@ - Should the site content be available to users that are not logged in?
    * @Public@ - yes.
    * @Private@ - no.
* @REGISTRATION@ - Should registration be available to anyone?
    * @OpenRegistration@ - yes.
    * @InvitesOnly@ - no.

-}

{-# language TypeApplications #-}

module Web.Bulletin.Config where

import qualified Data.Text as T
import qualified Control.Monad.Logger as Log
import qualified Database.Persist.Sqlite as PSqlite3
import qualified Web.Scotty.Sqlite.Users as Users
import System.Environment (lookupEnv)
import GHC.Conc (numCapabilities)


-- * General

-- | Configuration environment and state
data Config
  = Config
    { cfgEnv :: Environment -- ^ Working environment
    , cfgUsersStateInfo :: Users.StateInfo -- ^ @my-scotty-users@ state
    , cfgPort :: Port -- ^ Port
    , cfgVisibility :: Visibility -- ^ Public or private website
    , cfgRegistration :: Registration -- ^ Open or closed registration
    }

-- | Read environment variables and generate a config
getConfig :: Users.SessionStore -> IO Config
getConfig ses = do
  env <- getEnv
  visibility <- getVisibility
  reg <- getRegistration
  let
    mode = case env of
      Production -> Users.Production
      Development -> Users.Development
      Testing -> Users.Development
  pool <- getPool env
  port <- getPort env
  pure $ Config env (Users.StateInfo pool ses mode) port visibility reg

-- ** Visibility

-- | @VISIBLE@ env var: Should the site content be available to users that are not logged in?
data Visibility
  = Public -- ^ Yes.
  | Private -- ^ No.
  deriving (Eq, Read, Show, Enum, Bounded)

-- | Get the @VISIBLE@ variable. If none is provided uses @Public@
getVisibility :: IO Visibility
getVisibility =
  maybe Public readVisibility <$> lookupEnv "VISIBLE"

-- | Parse @Visibility@
readVisibility :: String -> Visibility
readVisibility str =
  case reads str of
    [(visible, "")] -> visible
    _ -> error $ unlines $
      [ "Could not parse: '" <> str <> "' as a valid environment options."
      , "Expecting one of the following:"
      ] <>
      map (("- " <>) . show @Visibility) [ minBound .. maxBound ]

-- ** Registration

-- | @REGISTRATION@ env var: Should registration be available to anyone?
data Registration
  = OpenRegistration -- ^ Yes.
  | InvitesOnly -- ^ No.
  deriving (Eq, Read, Show, Enum, Bounded)

-- | Get the @REGISTRATION@ variable. If none is provided uses @OpenRegistration@
getRegistration :: IO Registration
getRegistration =
  maybe OpenRegistration readRegistration <$> lookupEnv "REGISTRATION"

-- | Parse @Registration@
readRegistration :: String -> Registration
readRegistration str =
  case reads str of
    [(reg, "")] -> reg
    _ -> error $ unlines $
      [ "Could not parse: '" <> str <> "' as a valid environment options."
      , "Expecting one of the following:"
      ] <>
      map (("- " <>) . show @Registration) [ minBound .. maxBound ]

-- ** Environment

-- | @SCOTTY_ENV@ env var: server environment
data Environment
  = Development -- ^ One major thing to note here is that cookies are not flagged as @secure@ so it can be used without TLS
  | Production -- ^ Cookies are marked as secure so can only really work with TLS.
  | Testing -- ^ Not really in use at the moment, sorry for being lazy and not writing tests.
  deriving (Eq, Read, Show, Enum, Bounded)

-- | Get the @SCOTTY_ENV@ variable. If none is provided uses @Development@
getEnv :: IO Environment
getEnv =
  maybe Development readEnv <$> lookupEnv "SCOTTY_ENV"

-- | Parse environment
readEnv :: String -> Environment
readEnv str =
  case reads str of
    [(env, "")] -> env
    _ -> error $ unlines $
      [ "Could not parse: '" <> str <> "' as a valid environment options."
      , "Expecting one of the following:"
      ] <>
      map (("- " <>) . show @Environment) [ minBound .. maxBound ]

-- ** Port

type Port = Int

-- | Get the @PORT@ env var or use a default one according to the environment.
--
--   * @Development@ -> @8080@
--
--   * @Production@ -> @443@
--
--   * @Testing@ -> @8000@
getPort :: Environment -> IO Int
getPort env = do
  m <- lookupEnv "PORT"
  case m of
    Just mp ->
      case reads mp of
        [(port, [])] -> pure port
        _ -> error "Failed to read PORT environment option"
    Nothing ->
      pure $ case env of
        Development -> 8080
        Production -> 443
        Testing -> 8000

-- ** DB Connection

type ConnectionString = T.Text

-- | Get the connection string variable. This is mandatory.
--
--   See <https://www.sqlite.org/c3ref/open.html> for valid connection strings.
--
getConnectionString :: Environment -> IO ConnectionString
getConnectionString e =
  case e of
--    Development ->
--      pure $ T.pack ":memory:"
--    Test ->
--      pure $ T.pack ":memory:"
--    Production ->
    _ ->
      maybe (error "could not find parameter 'CONN_STRING'") T.pack <$> lookupEnv "CONN_STRING"

-- | Create the right sqlite3 connection pool according to the environment.
getPool :: Environment -> IO PSqlite3.ConnectionPool
getPool env = do
  cs <- getConnectionString env
  let
    poolSize = ceiling (fromIntegral numCapabilities / 2)
  case env of
    Development -> Log.runStderrLoggingT
      (PSqlite3.createSqlitePool cs poolSize)
    Production -> Log.runStderrLoggingT
      (PSqlite3.createSqlitePool cs poolSize)
    Testing -> Log.runNoLoggingT
      (PSqlite3.createSqlitePool cs poolSize)
